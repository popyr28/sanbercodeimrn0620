function arrayToObject(arr) {
    var now = new Date()
    var obj = {}
    var thisYear = now.getFullYear()
    n = arr.length
    if (n == 0){
        console.log(obj)
    } else {
        for(var i = 0; i < n; i++){
            ages = arr[i][3] == undefined || arr[i][3] > thisYear? "Invalid Birthday Year" : thisYear-arr[i][3];
            obj = {
                firstName : arr[i][0],
                lastName : arr[i][1],
                gender : arr[i][2],
                age : ages
            }
            console.log(`${i + 1}. ${obj.firstName} ${obj.lastName}:`)
            console.log(obj)
        }
    }
}
 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
arrayToObject([])

console.log("\n\n")

function shoppingTime(memberId = 0, money = 0) {
    moneyIn = money
    var get = []
    var par = [0, 0, 0, 0, 0]
    if (memberId == 0){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money <= 49999){
        return "Mohon maaf, uang tidak cukup"
    } else {
        while(money > 49999){
            if (money >= 1500000 && par[0] == 0){
                get.push("Sepatu Stacattu")
                money -= 1500000
                par[0] = 1;
            } else if (money >= 500000 && par[1] == 0){
                get.push("Baju Zoro")
                money -= 500000
                par[1] = 1;
            } else if (money >= 250000 && par[2] == 0){
                get.push("Baju H&N")
                money -= 250000
                par[2] = 1;
            } else if (money >= 175000 && par[3] == 0){
                get.push("Sweater Uniklooh")
                money -= 175000
                par[3] = 1;
            } else if (money >= 50000 && par[4] == 0){
                get.push("Casing Handphone")
                money -= 50000
                par[4] = 1
            }else{
                break;
            }
        }
        changedMoney = money
        var member = {
            memberId : memberId,
            money : moneyIn,
            listPurchased : get,
            changeMoney : changedMoney
        }
        return member
    }
}
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); 
  console.log(shoppingTime('234JdhweRxa53', 15000)); 
  console.log(shoppingTime());


  console.log("\n\n")
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    arr = []
    if (arrPenumpang.length == 0){
        return arr
    } else {
        for(var i = 0; i < arrPenumpang.length; i++){
            console.log(arrPenumpang[i][2])
            console.log(rute.indexOf(arrPenumpang[i][2]))
            bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))*2000
            var out = {
                penumpang : arrPenumpang[i][0],
                naikDari : arrPenumpang[i][1],
                tujuan : arrPenumpang[i][2],
                bayar : bayar
            } 
            arr.push(out)
        }
        return arr
    }
}
   
  //TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));   
console.log(naikAngkot([])); //[]