console.log("Soal1-Range");
function range(startNum = 0, finishNum = 0){
    var n = Math.abs(finishNum - startNum) + 1;
    var arr = [];
    if(startNum == 0 || finishNum == 0){
        arr = [-1];
    } else if(startNum < finishNum){
        for (i = 0; i < n; i++){
            arr[i] = startNum + i;
        }
    } else if(startNum > finishNum){
        for (i = 0; i < n; i++){
            arr[i] = startNum - i;
        }
    }
    return arr;
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log("\nSoal2-Range with Step");
function rangeWithStep(startNum = 0, finishNum = 0, step){
    var n = Math.abs(finishNum - startNum) + 1;
    var arr = [];
    if(startNum == 0 || finishNum == 0){
        arr = [-1];
    } else if(startNum < finishNum){
        var k = 0;
        for (i = 0; i < n; i+=step){
            arr[k] = startNum + i;
            k++;
        }
    } else if(startNum > finishNum){
        var k = 0;
        for (i = 0; i < n; i+=step){
            arr[k] = startNum - i;
            k++
        }
    }
    return arr;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3)); 
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("\nSoal3-Sum of Range");
function sum(startNum = 0, finishNum = 0, step = 1){
    var n = Math.abs(finishNum - startNum) + 1;
    var arr = [];
    if(startNum == 0 || finishNum == 0){
        if(startNum == 0)
            arr = [finishNum];
        else arr = [startNum];
    } else if(startNum < finishNum){
        var k = 0;
        for (i = 0; i < n; i+=step){
            arr[k] = startNum + i;
            k++;
        }
    } else if(startNum > finishNum){
        var k = 0;
        for (i = 0; i < n; i+=step){
            arr[k] = startNum - i;
            k++
        }
    }
    var jumlah = 0;
    for (j = 0; j < arr.length; j++){
        jumlah += arr[j];
    }
    return jumlah;
}
console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum()) 

console.log("/Soal4-Array Multidimensi");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(arr){
    n = arr.length;
    // m = arr[0].length;
    for(i = 0; i < n; i++){
        // for(j = 0; j < m; j++){
        //     console.log(arr[i][j]);
        // }
        console.log("Nomor ID : "+arr[i][0]);
        console.log("Nama Lengkap : "+arr[i][1]);
        console.log("TTL : "+arr[i][3]);
        console.log("Hobi : "+arr[i][4]);
        console.log("\n");
    }
}
dataHandling(input);

console.log("/Soal5-Balik Kata");
function balikKata(word){
    newWord = "";
    n = word.length;
    for (i = n; i > 0; i--){
        newWord += word[i-1];
    }
    return newWord;
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))


console.log("/Soal6-Metode Array");
function dataHandling2(array){
    console.log(splicing(array));
    date = spliting(array);
    console.log(getMonth(date));
    console.log(sortDesc(date));
    console.log(joinDate(array));
    slicing(array);
}
function splicing(arr){
    arr.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    arr.splice(4, 2, "Pria", "SMA Internasional Metro");
    return arr;
}

function slicing(array){
    nameArr = array[1];
    newName = nameArr.slice(0, 14);
    console.log(newName);
}

function spliting(arr1){
    var date = arr1[3];
    var newDate = date.split("/");
    return newDate;
}

function sortDesc(arr2){
    newArr = arr2.sort(function (value1, value2){return value2 - value1});
    return newArr;
}

function joinDate(date1){
    date2 = spliting(date1);
    newDate1 = date2.join("-");
    return newDate1;
}

function getMonth(date){
    var newDate = parseInt(date[1]);
    var month ="";
    switch (newDate){
        case 1 : {month = "Januari"; break;}
        case 2 : {month = "Febriari"; break;}
        case 3 : {month = "Maret"; break;}
        case 4 : {month = "April"; break;}
        case 5 : {month = "Mei"; break;}
        case 6 : {month = "Juni"; break;}
        case 7 : {month = "Juli"; break;}
        case 8 : {month = "Agustus"; break;}
        case 9 : {month = "September"; break;}
        case 10 : {month = "Oktober"; break;}
        case 11 : {month = "November"; break;}
        case 12 : {month = "Desember"; break;}
        default : {month = "gagal"; break;}
    }
    return month;
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);