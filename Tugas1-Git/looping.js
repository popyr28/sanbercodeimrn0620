console.log("NO.1 Looping While")
var i = 1;
console.log("LOOPING PERTAMA")
while (i <= 20){
    if(i % 2 == 0){
        console.log(i+" - I love coding");
    }
    i++;
}
console.log("\n");
var j = 20;
console.log("LOOPING KEDUA")
while (i > 0){
    if(i % 2 == 0){
        console.log(i+" - I will become a mobile developer");
    }
    i--;
}

console.log("\nNO.2 Looping for")
for(var i = 1; i <= 20; i++){
    if(i % 2 == 1){
        if(i % 3 == 0){
            console.log(i+ " - I Love Coding");
        }else{
            console.log(i+ " - Santai");
        }
    } else if(i % 2 == 0){
        console.log(i+ " - Berkualitas");
    }
}

console.log("\nNO.3 Membuat Persegi Panjang #")
var tagar = "";
for(var k = 0; k < 4; k++){
    for(var j = 0; j < 8; j++){
        tagar += "#";
    }
    tagar += "\n";
}
console.log(tagar); 

console.log("\nNO.4 Membuat Tangga")
var tangga = "";
for(var i = 1; i <= 8; i++){
    for(var j = 0; j < i; j++){
        tangga += "#";
    }
    tangga += "\n";
}
console.log(tangga);

console.log("\nNO.5 Membuat Papan Catur")
var catur = ""
for(var i = 1; i <= 8; i++){
    for(var j = 1; j <= 8; j++){
        if (i % 2 != 0 && j % 2 != 0 || i % 2 == 0 && j % 2 == 0){
            catur += " ";
        } else{
            catur += "#";
        }
    }
    catur += "\n";
}
console.log(catur);