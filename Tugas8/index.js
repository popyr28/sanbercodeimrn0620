var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
const letsRead = (index, time) => {
    index > books.length-1 ? '' :
        readBooks(time, books[index], time => {
            return index + letsRead(index+1, time)
        })
    }

letsRead(0, 10000)